drop table DRINKER;
create table DRINKER (
  DNAME varchar(50));

insert into DRINKER values
('John');
insert into DRINKER values
('Peter');
insert into DRINKER values
('Donald');
insert into DRINKER values
('Jeremy');
insert into DRINKER values
('Clark');

drop table BEER;
create table BEER (
  RNAME varchar(50),
  PRICE integer);

insert into BEER values
('Bud', 12);
insert into BEER values
('Michelob', 8);
insert into BEER values
('Fosters', 16);
insert into BEER values
('Heineken', 18);

drop table SERVES;
create table SERVES (
  BNAME varchar(50),
  RNAME varchar(50));

insert into SERVES values
('Jillians', 'Bud');
insert into SERVES values
('Jillians', 'Michelob');
insert into SERVES values
('Jillians', 'Heineken');
insert into SERVES values
('Dugans', 'Bud');
insert into SERVES values
('Dugans', 'Michelob');
insert into SERVES values
('Dugans', 'Fosters');
insert into SERVES values
('ESPN Zone', 'Fosters');
insert into SERVES values
('Charlies', 'Heineken');
insert into SERVES values
('Charlies', 'Foster');

drop table FREQUENTS;
create table FREQUENTS (
  DNAME varchar(50),
  BNAME varchar(50));

insert into FREQUENTS values
('John', 'Jillians');
insert into FREQUENTS values
('John', 'Dugans');
insert into FREQUENTS values
('Peter', 'ESPN Zone');
insert into FREQUENTS values
('Peter', 'Dugans');
insert into FREQUENTS values
('Donald', 'Dugans');
insert into FREQUENTS values
('Jeremy', 'Jillians');
insert into FREQUENTS values
('Jeremy', 'Dugans');
insert into FREQUENTS values
('Clark', 'ESPN Zone');
insert into FREQUENTS values
('Clark', 'Charlies');

drop table LIKES;
create table LIKES (
  DNAME varchar(50),
  RNAME varchar(50));

insert into LIKES values
('John', 'Bud');
insert into LIKES values
('John', 'Fosters');
insert into LIKES values
('John', 'Heineken');
insert into LIKES values
('Peter', 'Bud');
insert into LIKES values
('Donald', 'Bud');
insert into LIKES values
('Donald', 'Michelob');
insert into LIKES values
('Donald', 'Fosters');
insert into LIKES values
('Donald', 'Heineken');
insert into LIKES values
('Jeremy', 'Bud');
insert into LIKES values
('Jeremy', 'Fosters');
insert into LIKES values
('Jeremy', 'Heineken');
insert into LIKES values
('Clark', 'Bud');
insert into LIKES values
('Clark', 'Michelob');

drop table BAR;
create table BAR (
  BNAME varchar(50));

insert into BAR values
('Jillians');
insert into BAR values
('Dugans');
insert into BAR values
('ESPN Zone');
insert into BAR values
('Charlies'); 

