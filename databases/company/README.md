##Sample Company Database

There are three files for each table in this directory. For example for the
DEPARTMENT table the three files are: 


1. department.sql: contains the SQL Create Table statement to create the table.
2. department.dat: contains data for the table to be loaded using the MySQL load command.
3. load-department.sql: contains the LOAD command to load data into the table.

The file, source.sql, contains MySQL "source" commands to execute the "create table" and
"load" commands for each table.

After signing in to MySQL and changing to "company" database, simply run the following
command to create and populate the tables:

	mysql> source source.sql


