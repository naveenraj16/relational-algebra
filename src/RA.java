import java.io.*;
import java.util.*;
import java.lang.*;

public class RA {

  static boolean evalError = false;
  static int counter = 0;

  static public void main(String argv[]) throws IOException{    
    MySQL m = new MySQL();
    m.open(argv[0]);
    System.out.print("RA> ");
    do {
      String input = readInput().trim();
      if (input.equals("exit")) 
        break;
      else if(input.startsWith("source")){
	String fileName = input.substring(7);
        input = readFromFile(fileName);
      }
      else if(input.equals("schema")){
     	m.displayDatabaseSchema();
        continue;
      }
      input += ";";
      try {
        StringReader reader = new StringReader(input);
        parser p = new parser(new Lexer(reader));
        RANode tree = (RANode) p.parse().value;
	String result = semanticChecks(tree,m);
        if(!result.equals("OK"))
	  System.out.println(result);
	else{
	  //phase 3
	  evalError = false;
	  //reset counter 
    	  counter = 0;
 	  populateRelationNames(tree);
          //tree.printTree();
	  String query = generateSQL(tree,m);
 	  //if user puts -q print query along with results
	  if((argv.length > 1) &&(argv[1].equals("-q")))
            System.out.println("\nSQL query = "+query);
	  m.displayQueryResults(query,tree);
	}
      } catch (Exception e) {
          System.out.println("\nSYNTAX ERROR\n");
          e.printStackTrace();
        }
    } while (true); 
  }

//assigns names to subqueries to avoid mysql syntax error
public static void populateRelationNames(RANode tree){
  if(!tree.getRnodetype().equals("relation")){
    populateRelationNames(tree.getLchild());
    if(tree.getRchild() != null)
      populateRelationNames(tree.getRchild());
    tree.setRelationName("temp"+counter);    
    counter++;
  }
}

//given the relational algebra expression tree, generate an equivalent 
//mysql expression.
public static String generateSQL(RANode tree,MySQL m){
  if(tree.getRnodetype().equals("relation")){
    return "(select * from "+tree.getRelationName()+")";
  }
  else if(tree.getRnodetype().equals("union")){
    String lquery = generateSQL(tree.getLchild(),m);
    String rquery = generateSQL(tree.getRchild(),m);
    return lquery+" union "+rquery;
  }
  else if(tree.getRnodetype().equals("times")){
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String rquery = generateSQL(tree.getRchild(),m);
    if (tree.getRchild().getRnodetype().equals("union"))
      rquery = "("+rquery+")";
    return "(select * from "+lquery+" "+tree.getLchild().getRelationName()+ 
           ", "+rquery+" "+tree.getRchild().getRelationName()+")";  
  }
  else if(tree.getRnodetype().equals("project")){
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String query = "(select distinct ";
    for(int i = 0; i < tree.getAttributes().size()-1; i++){
      query += tree.getLchild().getRelationName()+"."+tree.getAttributes().get(i)+", ";
    } 
    query += tree.getAttributes().get(tree.getAttributes().size()-1);
    query += " from "+lquery+" "+tree.getLchild().getRelationName()+")";
    return query;
  }
  else if(tree.getRnodetype().equals("rename")){
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String query = "(select ";
    for(int i = 0; i < tree.getAttributes().size()-1; i++){
      query += tree.getLchild().getSchema().get(i)+" "+tree.getAttributes().get(i)+", ";
    } 
    query += tree.getLchild().getSchema().get(tree.getAttributes().size()-1)+" "+tree.getAttributes().get(tree.getAttributes().size()-1);
    query += " from "+lquery+tree.getLchild().getRelationName()+")";
    return query;
  }
  else if(tree.getRnodetype().equals("select")){
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String query = "(select * from "+lquery+" "+tree.getLchild().getRelationName()+" where ";
    for(int i = 0; i < tree.getConditions().size()-1; i++){
      query += tree.getConditions().get(i).toString()+" and ";
    } 
    query += tree.getConditions().get(tree.getConditions().size()-1).toString();
    query += ")";
    return query;
  }
  else if(tree.getRnodetype().equals("join")){
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String rquery = generateSQL(tree.getRchild(),m);
    if (tree.getRchild().getRnodetype().equals("union"))
      rquery = "("+rquery+")";
    String query = "(select distinct ";
    for(int i = 0; i < tree.getSchema().size()-1; i++){
      if(tree.getJoinColumns().contains(tree.getSchema().get(i)))
        query += tree.getLchild().getRelationName()+"."+tree.getSchema().get(i)+", ";
      else
        query += tree.getSchema().get(i)+", ";
    }
    if(tree.getJoinColumns().contains(tree.getSchema().get(tree.getSchema().size()-1)))
    query += tree.getLchild().getRelationName()+"."+tree.getSchema().get(tree.getSchema().size()-1);
    else  
      query += tree.getSchema().get(tree.getSchema().size()-1);
    query += " from "+lquery+" "+tree.getLchild().getRelationName()+", "+rquery+" "+tree.getRchild().getRelationName()+" ";
    if(tree.getJoinColumns().size() == 0)
      return query+")";
    query += " where ";
    for(int i = 0; i < tree.getJoinColumns().size()-1; i++){
      query += tree.getLchild().getRelationName()+"."+tree.getJoinColumns().get(i)+"="+
               tree.getRchild().getRelationName()+"."+tree.getJoinColumns().get(i)+" and ";
    } 
    query += tree.getLchild().getRelationName()+"."+tree.getJoinColumns().get(tree.getJoinColumns().size()-1)+"="+
             tree.getRchild().getRelationName()+"."+tree.getJoinColumns().get(tree.getJoinColumns().size()-1);
    query += ")";
    return query;
  }
  else if(tree.getRnodetype().equals("intersect")){
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String rquery = generateSQL(tree.getRchild(),m);
    if (tree.getRchild().getRnodetype().equals("union"))
      rquery = "("+rquery+")";
    String query = "(select * from "+lquery+" "+tree.getLchild().getRelationName()+" where (";
    for(int i = 0; i < tree.getSchema().size()-1; i++){
      query += tree.getSchema().get(i)+", ";
    } 
    query += tree.getSchema().get(tree.getSchema().size()-1) +") in ";
    query += "(select * from "+rquery+" "+tree.getRchild().getRelationName()+")";
    query += ")";
    return query;
  }
  else{//must be minus
    String lquery = generateSQL(tree.getLchild(),m);
    if (tree.getLchild().getRnodetype().equals("union"))
      lquery = "("+lquery+")";
    String rquery = generateSQL(tree.getRchild(),m);
    if (tree.getRchild().getRnodetype().equals("union"))
      rquery = "("+rquery+")";
    String query = "(select * from "+lquery+" "+tree.getLchild().getRelationName()+" where (";
    for(int i = 0; i < tree.getSchema().size()-1; i++){
      query += tree.getSchema().get(i)+", ";
    } 
    query += tree.getSchema().get(tree.getSchema().size()-1) +") not in ";
    query += "(select * from "+rquery+" "+tree.getRchild().getRelationName()+")";
    query += ")";
    return query;
  }
}

//perform semantic checks on the relational algebra query
public static String semanticChecks(RANode tree,MySQL m){
  if(tree.getRnodetype().equals("relation")){
    if(!m.relationExists(tree.getRelationName()))
      return tree.getRelationName()+" does not exist.";
    tree.setSchema(m.getAttributes(tree.getRelationName()));
    tree.setDataTypes(m.getDataTypes(tree.getRelationName()));
    return "OK";
  }
  else if(tree.getRnodetype().equals("union") ||
          tree.getRnodetype().equals("minus") ||
          tree.getRnodetype().equals("intersect")){
    String lresult = semanticChecks(tree.getLchild(),m);
    if(!lresult.equals("OK"))
      return lresult;
    String rresult = semanticChecks(tree.getRchild(),m);
    if(!rresult.equals("OK"))
      return rresult;
    ArrayList<String> leftSchema = tree.getLchild().getSchema();
    ArrayList<String> leftDataTypes = tree.getLchild().getDataTypes();
    ArrayList<String> rightSchema = tree.getRchild().getSchema();
    ArrayList<String> rightDataTypes = tree.getRchild().getDataTypes();
    if(leftSchema.size() != rightSchema.size())
      return "tables are not the same size";
    for(int i = 0; i < leftDataTypes.size(); i++){
      if(!leftDataTypes.get(i).equals(rightDataTypes.get(i)))
        return "columns do not have the same data types";
    }
    tree.setSchema(leftSchema);
    tree.setDataTypes(leftDataTypes);
    return "OK";
  }
  else if(tree.getRnodetype().equals("times")){
    String lresult = semanticChecks(tree.getLchild(),m);
    if(!lresult.equals("OK"))
      return lresult;
    String rresult = semanticChecks(tree.getRchild(),m);
    if(!rresult.equals("OK"))
      return rresult;
    ArrayList<String> leftSchema = tree.getLchild().getSchema();
    ArrayList<String> leftDataTypes = tree.getLchild().getDataTypes();
    ArrayList<String> rightSchema = tree.getRchild().getSchema();
    ArrayList<String> rightDataTypes = tree.getRchild().getDataTypes();
    ArrayList<String> schema = new ArrayList<String>(); 
    ArrayList<String> dataTypes = new ArrayList<String>(); 
    for(int i = 0; i < leftSchema.size(); i++){
      if(rightSchema.contains(leftSchema.get(i)))
	schema.add(tree.getLchild().getRelationName()+"."+leftSchema.get(i));
      else
   	schema.add(leftSchema.get(i));
      dataTypes.add(leftDataTypes.get(i));
    } 
    for(int i = 0; i < rightSchema.size(); i++){
      if(leftSchema.contains(rightSchema.get(i)))
	schema.add(tree.getRchild().getRelationName()+"."+rightSchema.get(i));
      else
   	schema.add(rightSchema.get(i));
      dataTypes.add(rightDataTypes.get(i));
    } 
    tree.setSchema(schema);
    tree.setDataTypes(dataTypes);
    return "OK";
  }
  else if(tree.getRnodetype().equals("join")){
    String lresult = semanticChecks(tree.getLchild(),m);
    if(!lresult.equals("OK"))
      return lresult;
    String rresult = semanticChecks(tree.getRchild(),m);
    if(!rresult.equals("OK"))
      return rresult;
    ArrayList<String> leftSchema = tree.getLchild().getSchema();
    ArrayList<String> leftDataTypes = tree.getLchild().getDataTypes();
    ArrayList<String> rightSchema = tree.getRchild().getSchema();
    ArrayList<String> rightDataTypes = tree.getRchild().getDataTypes();
    ArrayList<String> schema = new ArrayList<String>(); 
    ArrayList<String> dataTypes = new ArrayList<String>(); 
    ArrayList<String> joinColumns = new ArrayList<String>();
    for(int i = 0; i < leftSchema.size(); i++){
      schema.add(leftSchema.get(i));
      dataTypes.add(leftDataTypes.get(i));
      if(rightSchema.contains(leftSchema.get(i)))
	joinColumns.add(leftSchema.get(i));
    } 
    for(int i = 0; i < rightSchema.size(); i++){
      if(!leftSchema.contains(rightSchema.get(i))){
	schema.add(rightSchema.get(i));
	dataTypes.add(rightDataTypes.get(i));
      }
    } 
    tree.setJoinColumns(joinColumns);
    tree.setSchema(schema); 
    tree.setDataTypes(dataTypes);
    return "OK";
  }
  else if(tree.getRnodetype().equals("project")){
    String lresult = semanticChecks(tree.getLchild(),m);
    if(!lresult.equals("OK"))
      return lresult;
    ArrayList<String> attributes = tree.getAttributes();
    ArrayList<String> schema = tree.getLchild().getSchema();
    ArrayList<String> dataTypes = tree.getLchild().getDataTypes();
    ArrayList<String> schemaDataTypes = new ArrayList<String>(); 
    for(int i = 0; i < attributes.size(); i++){
      if(!schema.contains(attributes.get(i)))
    	return attributes.get(i)+" is not a valid attribute";
    }
    for(int i = 0; i < attributes.size(); i++){
      int index = schema.indexOf(attributes.get(i));
      schemaDataTypes.add(dataTypes.get(index));
    }
    tree.setSchema(attributes); 
    tree.setDataTypes(schemaDataTypes); 
    return "OK";
  }
  else if(tree.getRnodetype().equals("rename")){
    String lresult = semanticChecks(tree.getLchild(),m);
    if(!lresult.equals("OK"))
      return lresult;
    ArrayList<String> attributes = tree.getAttributes();
    ArrayList<String> schema = tree.getLchild().getSchema();
    ArrayList<String> dataTypes = tree.getLchild().getDataTypes();
    if(schema.size() != attributes.size())
      return "Not a valid amount of attributes";
    tree.setSchema(attributes); 
    tree.setDataTypes(dataTypes); 
    return "OK";
  }
  //select node
  else{
    String lresult = semanticChecks(tree.getLchild(),m);
    if(!lresult.equals("OK"))
      return lresult;
    ArrayList<String> schema = tree.getLchild().getSchema();
    ArrayList<String> dataTypes = tree.getLchild().getDataTypes();
    ArrayList<Condition> conditions = tree.getConditions();
    for(int i = 0; i < conditions.size(); i++){
      String leftOperand = conditions.get(i).getLeftOperand();
      String rightOperand = conditions.get(i).getRightOperand();
      String leftDataType = conditions.get(i).getLeftDataType();
      String rightDataType = conditions.get(i).getRightDataType();
      //check to see if left operand column name exists
      if(leftDataType.equals("NAME") && schema.indexOf(leftOperand) == -1)
	return leftOperand+" is not a valid operand";
      //check to see if right operand column name exists
      if(rightDataType.equals("NAME") && schema.indexOf(rightOperand) == -1)
	return rightOperand+" is not a valid operand";
      if(leftDataType.equals("NAME")){
	int index = schema.indexOf(leftOperand);
	leftDataType = dataTypes.get(index);
      }	      
      if(rightDataType.equals("NAME")){
	int index = schema.indexOf(rightOperand);
	rightDataType = dataTypes.get(index);
      }	      
      if(!leftDataType.equals(rightDataType))
	return "Data types do not match";
    }
    tree.setSchema(schema);
    tree.setDataTypes(dataTypes);
    return "OK";
  }
}

static String readInput() {
     try {
       StringBuffer buffer = new StringBuffer();
       System.out.flush();
       int c = System.in.read();
       while(c != ';' && c != -1) {
         if (c != '\n') 
           buffer.append((char)c);
         else {
           buffer.append(" ");
           System.out.print("RA> ");
           System.out.flush();
         }
         c = System.in.read();
       }
       return buffer.toString().trim();
     } catch (IOException e) {
         return "";
       }
   }


public static String readFromFile(String fileName) throws IOException{
  File file = new File(fileName);
  FileReader fr = new FileReader(file);
  String input = "";
  int c = fr.read();
  while(c != -1){
    char ch = (char)c;
    if(ch == '/'){
      c = fr.read();
      ch = (char)c;
      if(ch == '/'){
	while(ch != '\n'){
	  c = fr.read();
          ch = (char)c;
   	}
      }
    }
    if(ch == '\n')
      ch = ' ';
    if(ch == ';')
      break;
    input += ch;
    c = fr.read();
  }
  return input;
}

}
