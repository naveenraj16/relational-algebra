public class MySQLDriver {
  public static void main(String[] args) {
    MySQL m = new MySQL();
    m.open(args[0]);
    m.displayDatabaseSchema();
    m.close();
  }
}

