import java.util.*;

public class RANode{
  public RANode lchild;
  public RANode rchild;  // will be null for unary operators
  public String rnodetype; // sel, ren, pro, uni, joi, min
  public String relationName;
  public ArrayList<String> attributes; // used with pro and ren
  public ArrayList<Condition> conditions;  
  public ArrayList<String> joinColumns; // used to remember JOIN columns
  public ArrayList<String> schemaColumns;
  public ArrayList<String> schemaDataTypes;

  public int sizeOfTree() {
    if ((lchild==null) && (rchild==null))
      return 1;
    else if (rchild==null)
      return (1+lchild.sizeOfTree());
    else
      return (1+lchild.sizeOfTree()+rchild.sizeOfTree());
  }

public RANode() {
  lchild = null;
  rchild = null;
  conditions = new ArrayList<Condition>();
  schemaColumns = new ArrayList<String>();
  schemaDataTypes = new ArrayList<String>();
  joinColumns = new ArrayList<String>();
}

public String toString(){

 int j=1;
 StringBuffer sbf = new StringBuffer();
 RANode tree = this;
 while(tree!=null){

                sbf.append("::::Node"+j+":");
                sbf.append("Node Type: "+tree.getRnodetype()+"  ");
                sbf.append("Relation Name "+tree.getRelationName()+"  ");
                sbf.append("Atributes are : [ ");
                ArrayList<String> vec = tree.getAttributes();
                if(vec!=null){

                        for(int i=0;i<vec.size();i++){

                               sbf.append((String)vec.get(i)+",");
                        }
                }else{

                        sbf.append("null ]  ");

                }

                tree = tree.getLchild();
                j++;

        }


        return sbf.toString();
}
/**
 * @return
 */
public ArrayList<String> getAttributes() {
  return attributes;
}
public ArrayList<String> getJoinColumns() {
  return joinColumns;
}

/**
 * @return
 */
public RANode getLchild() {
  return lchild;
}

/**
 * @return
 */
public RANode getRchild() {
  return rchild;
}

/**
 * @return
 */
public String getRelationName() {
  return relationName;
}


/**
 * @return
 */
public String getRnodetype() {
  return rnodetype;
}

public ArrayList<Condition> getConditions() {
  return conditions;
}

/**
 * @return
 */
public ArrayList<String> getSchema() {
  return schemaColumns;
}

public ArrayList<String> getDataTypes() {
  return schemaDataTypes;
}

/**
 * @param vector
 */
public void setAttributes(ArrayList<String> vector) {
  attributes = vector;
}

/**
 * @param node
 */
public void setLchild(RANode node) {
  lchild = node;
}


/**
 * @param node
 */
public void setRchild(RANode node) {
  rchild = node;
}

/**
 * @param string
 */
public void setRelationName(String string) {
  relationName = string;
}


/**
 * @param string
 */
public void setRnodetype(String string) {
  rnodetype = string;
}

public void setConditions(ArrayList<Condition> c) {
  conditions = c;
}

/**
 * @param vector
 */
public void setSchema(ArrayList<String> a) {
  schemaColumns = a;
}
public void setDataTypes(ArrayList<String> a) {
  schemaDataTypes = a;
}
public void setJoinColumns(ArrayList<String> a) {
  joinColumns = a;
}


public void printTree() {
   if ((lchild == null) && (rchild == null)) {
     System.out.println("Relation Name is : " + relationName);
     System.out.println("Schema is : " + schemaColumns);
     System.out.println("Datatypes is : " + schemaDataTypes);
   }
   else if (rchild == null) { // project or rename or select
     if (rnodetype.equals("project") || rnodetype.equals("rename")) {
       System.out.println("::::Node:");
       System.out.println("Node Type: " + rnodetype + "  ");
       System.out.println("Atributes are : [ ");
       ArrayList<String> vec = attributes;
       if(vec!=null){
         for(int i=0;i<vec.size();i++)
           System.out.println((String)vec.get(i)+",");
       } else
           System.out.println("null ] ");
       System.out.println("]");
       System.out.println("Schema is : " + schemaColumns);
       System.out.println("Datatypes is : " + schemaDataTypes);
       lchild.printTree();
    } else if (rnodetype.equals("select")) {
       System.out.println("Select Node");
       for (int i=0; i<conditions.size(); i++) {
         System.out.print(conditions.get(i).getLeftOperand());
         System.out.print(conditions.get(i).getLeftDataType());
         System.out.print("  ");
         System.out.print(conditions.get(i).getOperator());
         System.out.print("  ");
         System.out.println(conditions.get(i).getRightOperand());
         System.out.print(conditions.get(i).getRightDataType());
       }
       System.out.println("Schema is : " + schemaColumns);
       System.out.println("Datatypes is : " + schemaDataTypes);
       lchild.printTree();
      }
   } else {// union, minus, join
       if (rnodetype.equals("union") || rnodetype.equals("minus") || rnodetype.equals("join") ||
           rnodetype.equals("intersect") || rnodetype.equals("times")) {
         System.out.println("::::Node:");
         System.out.println("Node Type: "+rnodetype+"  ");
         System.out.println("Schema is : " + schemaColumns);
         System.out.println("Datatypes is : " + schemaDataTypes);
         lchild.printTree();
         rchild.printTree();
       }
     }
  }


}
