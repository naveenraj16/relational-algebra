import java_cup.runtime.*;
%%
%class Lexer
%line
%column
%cup
%{   
  private Symbol symbol(int type) {
    return new Symbol(type, yyline, yycolumn);
  }
  private Symbol symbol(int type, Object value) {
    return new Symbol(type, yyline, yycolumn, value);
  }
%}
LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]
Project = [pP][rR][oO][jJ][eE][cC][tT]
Rename = [rR][eE][nN][aA][mM][eE]
Union = [uU][nN][iI][oO][nN]
Minus = [mM][iI][nN][uU][sS]
Intersect = [iI][nN][tT][eE][rR][sS][eE][cC][tT]
Join = [jJ][oO][iI][nN]
Times = [tT][iI][mM][eE][sS]
Select = [sS][eE][lL][eE][cC][tT]
And = [aA][nN][dD]
Name = [a-zA-Z][a-zA-Z0-9_]*
Numeric_constant = [0-9]+ | [0-9]+"."[0-9]* | "."[0-9]*
String_constant = ['][^'\n]*[']
Comparison = "=" | "<>" | "<" | ">" | "<=" | ">="
%%
/* ------------------------Lexical Rules Section---------------------- */
<YYINITIAL> {
    ";"                { return symbol(sym.SEMI); }
    {Comparison}       { return symbol(sym.COMPARISON, new String(yytext())); }
    {Project}           { return symbol(sym.PROJECT); }
    {Union}           { return symbol(sym.UNION); }
    {Minus}           { return symbol(sym.MINUS); }
    {Intersect}           { return symbol(sym.INTERSECT); }
    {Join}           { return symbol(sym.JOIN); }
    {Times}           { return symbol(sym.TIMES); }
    {Rename}           { return symbol(sym.RENAME); }
    {Select}           { return symbol(sym.SELECT); }
    {And}           { return symbol(sym.AND); }
    "]"           { return symbol(sym.RBRACK); }
    "["           { return symbol(sym.LBRACK); }
    ")"           { return symbol(sym.RPAREN); }
    "("           { return symbol(sym.LPAREN); }
    ","           { return symbol(sym.COMMA); }
    {Name}          { return symbol(sym.NAME, new String(yytext())); }
    {Numeric_constant}       { return symbol(sym.NUMBER,new String(yytext())); }
    {String_constant}        { return symbol(sym.STRING,new String(yytext())); }
    {WhiteSpace}       { /* just skip what was found, do nothing */ }   
}
[^]                    { System.out.println("Syntax Error - Scanning Error");
                         return symbol(sym.ERROR); }
