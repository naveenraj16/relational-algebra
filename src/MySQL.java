import java.io.*;
import java.util.*;
import java.sql.*;
import com.mysql.jdbc.ResultSetImpl;

public class MySQL {

  static ArrayList<String> relations;
  static HashMap<String,ArrayList<String>> attributes;
  static HashMap<String,ArrayList<String>> dataTypes;

  static String driver;
  static String connStr;
  static String dbName;
  static String usr;
  static String pwd;

  static Connection conn;

  public MySQL() {
    relations = new ArrayList<String>();
    attributes = new HashMap<String,ArrayList<String>>();
    dataTypes = new HashMap<String,ArrayList<String>>();
    conn = null;
  }

  public static void open(String configFile) {
    setMySQLConfigParameters(configFile);
    try {
      Class.forName(driver);
    } catch (ClassNotFoundException e) {
        System.out.println ("Could not load the driver");
      }
    try {
      String cstr = connStr+dbName+"?user="+usr+"&password="+pwd;
      conn = DriverManager.getConnection(cstr);
    } catch (SQLException e) {
        System.out.println ("Could not connect to the database");
        e.printStackTrace();
      }
    try {
      initializeSchema();
    } catch (SQLException e) {
        System.out.println ("Could not initialize database");
        System.out.println(e.getMessage());
      }
  }

  public static void close() {
    try {
      conn.close();
    } catch (SQLException e) {
        System.out.println ("Could not close database");
      }
  }

  public static void initializeSchema() throws SQLException {

    Statement stmt = conn.createStatement ();
    String query = "select distinct table_name from information_schema.tables "+
                   "where table_schema='"+dbName+"'";
    ResultSet rset = stmt.executeQuery(query);
    while (rset.next ()) {
      relations.add(rset.getString(1).toUpperCase());
    }
    for (int i=0; i<relations.size(); i++) {
      String rname = (String) relations.get(i);
      query = "select distinct column_name,column_type from information_schema.columns "+
         "where table_name='"+rname+"'";
      rset = stmt.executeQuery(query);
      ArrayList<String> attrs = new ArrayList<String>();
      ArrayList<String> dtypes = new ArrayList<String>();
      while (rset.next ()) {
        attrs.add(rset.getString(1).toUpperCase());
        String dtype = null;
        if (rset.getString(2).startsWith("int"))
          dtype = "NUMBER";
        else if (rset.getString(2).startsWith("decimal"))
          dtype = "NUMBER";
        else
          dtype = "STRING";
        dtypes.add(dtype);
      }
      attributes.put(rname,attrs);
      dataTypes.put(rname,dtypes);
    }
  }

  public boolean relationExists(String rname) {
    return (relations.indexOf(rname) >= 0);
  }

  public ArrayList<String> getAttributes(String rname) {
    ArrayList<String> attrs =  attributes.get(rname);
    return attrs;
  }

  public ArrayList<String> getDataTypes(String rname) {
    ArrayList<String> dtypes = dataTypes.get(rname);
    return dtypes;
  }

  public void displayDatabaseSchema() {
    System.out.println("*********************************************");
    for (int i=0; i<relations.size(); i++) {
      String rname = relations.get(i);
      System.out.print(rname+"(");
      ArrayList<String> attrs = attributes.get(rname);
      ArrayList<String> dtypes = dataTypes.get(rname);
      for (int j=0; j<attrs.size(); j++) {
        String aname = attrs.get(j);
        String atype = dtypes.get(j);
        if (j == attrs.size()-1)
          System.out.println(aname+":"+atype+")");
        else
          System.out.print(aname+":"+atype+",");
      }
    }
    System.out.println("");
    System.out.println("*********************************************");
  }

  public void displayQueryResults(String query, RANode root) throws SQLException {
//    // Get count of tuples in answer
//    String countQuery = "(select count(*) from "+query+" queryTemp)";
//System.out.println("countQuery="+countQuery);
    Statement stmt = conn.createStatement ();
//    ResultSet rset = stmt.executeQuery(countQuery);
//    int numTuples = 0;
//    if (rset.next())
//      numTuples = rset.getInt(1);
    System.out.print("\nANSWER(");
    int nCols = root.getSchema().size();
    for (int i=0; i<nCols; i++) {
      String var = root.getSchema().get(i);
      String varType = root.getDataTypes().get(i);
      if (i == (nCols-1))
        System.out.println(var+":"+varType+")");
      else
        System.out.print(var+":"+varType+",");
    }
    // execute the query against MySQL database
ResultSet    rset = stmt.executeQuery(query);
double rowCount = ((ResultSetImpl)rset).getUpdateCount();
    System.out.println("\nNumber of tuples = "+(int)rowCount+"\n");
    while (rset.next ()) {
      for (int i=1; i<=nCols; i++) {
        if (i == (nCols))
          System.out.println(rset.getString(i)+":");
        else
          System.out.print(rset.getString(i)+":");
      }
    }
    System.out.println("");
    stmt.close();
  }



  public boolean isQueryResultEmpty(String query) throws SQLException {
    Statement stmt = conn.createStatement();
    ResultSet rset = stmt.executeQuery(query);
    stmt.close();
    return rset.next();
  }

  
  static void setMySQLConfigParameters(String fname) {
    InputStream fin=null;
    try {
        fin = new FileInputStream(fname);
      } catch (Exception e) {
          System.out.println("Could not open file: "+fname);
        }
    String line = readLine(fin);
    driver = line.substring(line.indexOf("=")+1);
    line = readLine(fin);
    connStr = line.substring(line.indexOf("=")+1);
    line = readLine(fin);
    dbName = line.substring(line.indexOf("=")+1);
    line = readLine(fin);
    usr = line.substring(line.indexOf("=")+1);
    line = readLine(fin);
    pwd = line.substring(line.indexOf("=")+1);
    fin = null;
  }

  static String readLine(InputStream fin) {
    String line="";
  char ch=' ';
    try {
      ch = (char) fin.read();
    } catch (Exception e) {
        System.out.println("Cannot read MySQL configuration file");
      }
    while (ch != '\n') {
      line += ch;
      try {
        ch = (char) fin.read();
      } catch (Exception e) {
          break;
        }
    }
    return line;
  }

}
